#!/bin/bash

# This script is used to relink runme.py file to currently opened file.
# Makes symlink from runme.py -> current opened file

# Setup in PyCharm:
# In Run/Debug configurations:
#   Script path: point to the runme.py file
# In External Tool create tool named "relink runme file":
#   Program: point to this file (relink_runme_file.sh)
#   Arguments: "$ProjectFileDir$/runme.py" "$FilePath$"
# Do not forget to add created tool (relink input file) to the Before launch list.

echo "Running relink_runme_file"
RunmeFile="$1" # file that is in common run configuration (runme.py path).
PythonFilePath="$2" # Current (opened in ide) file path and name

filename=$(basename -- "$PythonFilePath")
extension="${filename##*.}"
filebase="${filename%.*}"
filedir="$(dirname "$PythonFilePath")"

if [ "$extension" != "py" ]; then
    echo "Not a python file, exitting"
    exit 1
fi

rm -fv "$RunmeFile"
ln -sfv "$PythonFilePath" "$RunmeFile"

echo "Finished relink_runme_file"
