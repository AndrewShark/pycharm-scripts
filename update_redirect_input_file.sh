#!/bin/bash

# This script is used to change configuration to use the needed file as stdin input
# Places the current_python_basename.txt file path to the "Redirect input from" field.

# Setup in PyCharm:
# In Run/Debug configuration called "current:" (here name is important (not just for your use), it should start with curr.):
#   Redirect input from: do not forget to activate checkbox.
# In External Tool create tool named "update redirect input file":
#   Program: point to this file (pycharm-scripts/update_redirect_input_file.sh)
#   Arguments: "$ModuleFileDir$" "$ContentRoot$" "$FilePathRelativeToProjectRoot$"
#   Working directory: $FileDir$
# Do not forget to add created tool (update redirect input file) to the Before launch list.

idea_dir="$1"
project_path="$2"
cur_file_from_project="$3"

echo "$idea_dir $project_path $cur_file_from_project"

if [ ! -d "$idea_dir" ]; then
    echo "idea directory does not exist" >&2
    exit 1
elif [ ! -f "$idea_dir/workspace.xml"  ]; then
    echo "workspace.xml does not exist" >&2
    exit 1 
fi

filename=$(basename -- "$cur_file_from_project")
extension="${filename##*.}"
filebase="${filename%.*}"
filedir="$(dirname "$cur_file_from_project")"
task_input_file="$filedir/$filebase.txt"

if [ "$extension" != "py" ]; then
    if [ -f "$filebase".py ]; then
        echo "You selected \"$filename\". Assuming you wanted to choose \"$filebase.py\"."
        filename="$filebase".py
    else
        echo "Not a python file, exitting"
        exit 1
    fi
fi

echo "$task_input_file"

if [ ! -f "$project_path"/"$task_input_file" ]; then
    echo "Creating $project_path/$task_input_file"
    touch "$project_path/$task_input_file"
fi

echo ready
newval='$PROJECT_DIR$'/"$task_input_file"
echo "Setting new value: $newval"
# xmlstarlet ed --inplace -u "project/component[@name='RunManager']/configuration[@name='current opened py']/option[@name='INPUT_FILE']/@value" -v "$newval" "$idea_dir/workspace.xml" # If you want to hardcode the config name.
xmlstarlet ed --inplace -u "project/component[@name='RunManager']/configuration[starts-with(@name, 'curr')]/option[@name='INPUT_FILE']/@value" -v "$newval" "$idea_dir/workspace.xml"
echo finished
