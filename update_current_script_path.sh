#!/bin/bash

# This script is used to change configuration to use currently opened python file.
# Places the current_python.py file path to the "Script path" field.

# Setup in PyCharm:
# In Run/Debug configuration called "current:" (here name is important (not just for your use), it should start with curr.):
# In External Tool create tool named "update current script path":
#   Program: point to this file (pycharm-scripts/update_current_script_path.sh)
#   Arguments: "$ModuleFileDir$" "$FileDirRelativeToProjectRoot$" "$FileName$"
#   Working directory: $FileDir$
# Do not forget to add created tool (update current script path) to the Before launch list.

idea_dir="$1"
filedir_from_project_root="$2"
filename="$3"

if [ ! -d "$idea_dir" ]; then
    echo "idea directory does not exist" >&2
    exit 1
elif [ ! -f "$idea_dir/workspace.xml"  ]; then
    echo "workspace.xml does not exist" >&2
    exit 1
fi

extension="${filename##*.}"
filebase="${filename%.*}"

if [ "$extension" != "py" ]; then
    if [ -f "$filebase".py ]; then
        echo "You selected \"$filename\". Assuming you wanted to choose \"$filebase.py\"."
        filename="$filebase".py
    else
        echo "Not a python file, exitting"
        exit 1
    fi
fi

echo ready
oldval=$(xmlstarlet sel --encode utf-8 -t -v "project/component[@name='RunManager']/configuration[starts-with(@name, 'curr')]/option[@name='SCRIPT_NAME']/@value" "$idea_dir/workspace.xml")
newval='$PROJECT_DIR$'/"$filedir_from_project_root/$filename"
echo "Setting new value: $newval"
# xmlstarlet ed --inplace -u "project/component[@name='RunManager']/configuration[@name='current opened py']/option[@name='SCRIPT_NAME']/@value" -v "$newval" "$idea_dir/workspace.xml" # If you want to hardcode the config name.
xmlstarlet ed --inplace -u "project/component[@name='RunManager']/configuration[starts-with(@name, 'curr')]/option[@name='SCRIPT_NAME']/@value" -v "$newval" "$idea_dir/workspace.xml"

proceed_state_file="$idea_dir/.proceed" # This file is used later by run_expected_at_config_update.sh
echo -e "Compare:\nold: $oldval\nnew: $newval"
if [ "$oldval" == "$newval" ]; then
    echo "Writing \"yes\" to $proceed_state_file"
    echo "yes" > "$proceed_state_file"
else
    echo "Writing \"no\" to $proceed_state_file"
    echo "no" > "$proceed_state_file"
fi
echo finished
