#!/bin/bash

# This script is used to relink the file used as stdin input
# Makes symlink from input.txt -> current opened task.txt

# Setup in PyCharm:
# In Run/Debug configurations:
#   Redirect input from: point to the input.txt file
# In External Tool create tool named "relink input file":
#   Program: point to this file (relink_input_file.sh)
#   Arguments: "$ProjectFileDir$/input.txt" "$FilePath$"
# Do not forget to add created tool (relink input file) to the Before launch list.

echo "Running relink_input_file"
InputFile="$1" # file from which to make a link (input.txt path).
PythonFilePath="$2" # Current file path and name

filename=$(basename -- "$PythonFilePath")
extension="${filename##*.}"
filebase="${filename%.*}"
filedir="$(dirname "$PythonFilePath")"

if [ "$extension" != "py" ]; then
    echo "Not a python file, exitting"
    exit 1
fi

task_input_file="$filedir/$filebase.txt"

# Create the task input file in case id does not exist
if [ ! -f "$task_input_file" ]; then
    echo "Creating $task_input_file"
    touch "$task_input_file"
fi

rm -fv "$InputFile"
ln -sfv "$task_input_file" "$InputFile"

echo "Finished relink_input_file"
